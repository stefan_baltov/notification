package org.sbaltov.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//swagger endpoint is /swagger-ui/
// note ending slash

@SpringBootApplication
public class SmsMessageHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsMessageHandlerApplication.class, args);
	}

}

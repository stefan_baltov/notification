package org.sbaltov.sms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "rabbitmq.settings")
public class RabbitMqSettings {
	private String host;
	private int port;
	private String exchange;
	private String queue;
	private String user;
	private String password;
	private String type;
}

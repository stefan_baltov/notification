package org.sbaltov.sms.service;

import org.sbaltov.common.AbstractMessageSender;
import org.sbaltov.common.dto.MessageDto;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author sbaltov.
 * @version 0.1.0
 */

@Component
@RequiredArgsConstructor
public class Receiver extends AbstractMessageSender {

	private final RabbitTemplate rabbitTemplate;

	public void receiveMessage(MessageDto message) throws Exception {
		System.out.println("Received <" + message + ">");
		try {
			sendMessage(message);
			rabbitTemplate.convertAndSend(message);
		} catch (Exception e) {
			throw new AmqpRejectAndDontRequeueException(e.getMessage());
		}
	}

	@Override
	public void sendMessage(final MessageDto dto) throws Exception {
		//TODO Send a SMS to sms gateway
	}
}

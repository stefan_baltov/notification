package org.sbaltov.common.dto;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto for messages.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {
	@NotEmpty
	private String type;
	@NotEmpty
	private String address;
	@NotEmpty
	private String message;

	private String sender;

	@ApiModelProperty(hidden = true)
	private Long id;

	@ApiModelProperty(hidden = true)
	private MessageStatus status;
}

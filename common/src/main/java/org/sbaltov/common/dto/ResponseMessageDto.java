package org.sbaltov.common.dto;

import lombok.Data;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Data
public class ResponseMessageDto {
	private Integer code;
	private String reason;
}

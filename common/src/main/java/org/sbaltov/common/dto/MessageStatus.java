package org.sbaltov.common.dto;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
public enum MessageStatus {
	PENDING, SEND, ERROR
}

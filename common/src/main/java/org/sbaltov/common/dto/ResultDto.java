package org.sbaltov.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultDto {
	private Integer result;

}

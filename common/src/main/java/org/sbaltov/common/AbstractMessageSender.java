package org.sbaltov.common;

import org.sbaltov.common.dto.MessageDto;

/**
 * Abstract class for message senders.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
public abstract class AbstractMessageSender {

	public abstract void sendMessage(MessageDto dto) throws Exception;
}


create table if not exists messages
(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    msg_type varchar(100) ,
    address varchar ,
    message varchar ,
    sender varchar ,
    status varchar(100),
    cou integer ,
    created timestamp,
    modified timestamp,
    log varchar
    );

package org.sbaltov.core.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * This is an global error dto for errors.
 */
@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Slf4j
public class ErrorDto {
	private final ErrorType type;

	private String description;

	private List<String> fieldNames;

	private Map<String, Object> cause;

	private final String path;

	private String stackTrace;

	public ErrorDto(final ErrorType type, final String description, final String path) {
		log.debug("Create error dto with params: {} {} {}", type, description, path);
		this.type = type;
		this.description = description;
		this.path = path;
	}
}

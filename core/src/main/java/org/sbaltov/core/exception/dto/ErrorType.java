package org.sbaltov.core.exception.dto;

/**
 * Enum for holding possible values for a global error that may occur in any Phoenix microservice.
 */
public enum ErrorType {
	GENERAL_ERROR, 
	VALIDATION_ERROR, 
	RESOURCE_NOT_FOUND, 
	FORBIDDEN_ACCESS, 
	INTEGRATION_ERROR,
	PAYLOAD_TOO_LARGE
}

package org.sbaltov.core.exception;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.sbaltov.core.exception.dto.ErrorDto;
import org.sbaltov.core.exception.dto.ErrorType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

/**
 * Global exception handler. Reports all errors in a custom and more readable format.
 */
@Slf4j
@ControllerAdvice
public class GlobalControllerExceptionHandler {

	@Value("${errors.stack-trace-limit:10}")
	private Integer stackTraceLimit = 1; // default value for mocked unit tests

	/**
	 * This method handles all uncaught exceptions which are not handled by any other method.
	 *
	 * @param request   The request that generated the error.
	 * @param exception The exception thrown during the execution.
	 * @return {@link ErrorResponse} the error response
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Throwable.class)
	public ErrorResponse handleGeneralExceptions(final HttpServletRequest request, final Throwable exception) {
		return this.createErrorResponse(ErrorType.GENERAL_ERROR, request, exception);
	}

	/**
	 * This method handles all models validation exceptions.
	 *
	 * @param request   The request that generated the error.
	 * @param exception The validation exception.
	 * @return {@link ErrorResponse} the error response
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ErrorResponse handleValidationException(final HttpServletRequest request,
			final MethodArgumentNotValidException exception) {
		return buildValidationErrorResponse(exception.getBindingResult(), request.getRequestURI());
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ValidationException.class)
	public ErrorResponse handleValidationException(final HttpServletRequest request,
			final ValidationException exception) {
		return buildValidationErrorResponse(exception.getMessage(), request.getRequestURI());
	}

	/**
	 * This method handles bind validation exceptions.
	 *
	 * @param request   The request that generated the error.
	 * @param exception The validation exception.
	 * @return {@link ErrorResponse} the error response
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BindException.class)
	public ErrorResponse handleValidationException(final HttpServletRequest request, final BindException exception) {
		return buildValidationErrorResponse(exception.getBindingResult(), request.getRequestURI());
	}

	private ErrorResponse buildValidationErrorResponse(final String message, final String requestURI) {

		final ErrorDto errorDto = new ErrorDto(ErrorType.VALIDATION_ERROR, message, requestURI);

		return new ErrorResponse(errorDto);
	}

	/**
	 * Creates default error response out of a {@link Throwable}.
	 *
	 * @param errorType The error type.
	 * @param request   The request.
	 * @param throwable The throwable.
	 * @return {@link ErrorResponse} The error response
	 */
	private ErrorResponse createErrorResponse(final ErrorType errorType, final HttpServletRequest request,
			final Throwable throwable) {
		final String exceptionStackTrace = ExceptionUtils.getStackTrace(throwable);
		log.error("Error '{}' while processing REST service '{}'. Exception {}", throwable.getMessage(),
				request.getRequestURI(), exceptionStackTrace);
		final String message = Arrays.stream(ExceptionUtils.getRootCauseStackTrace(throwable))
				.limit(stackTraceLimit)
				.collect(Collectors.joining(","));
		ErrorDto errorDto = new ErrorDto(errorType, throwable.getMessage(), request.getRequestURI());
		errorDto.setStackTrace(message);
		return new ErrorResponse(errorDto);
	}

	/**
	 * Processes binding result.
	 *
	 * @param bindingResult The {@link BindingResult} instance to process
	 * @param requestURI    The request URI
	 * @return An {@link ErrorResponse} instance
	 */
	private ErrorResponse buildValidationErrorResponse(final BindingResult bindingResult, final String requestURI) {
		final List<String> errorMessages = new ArrayList<>();
		final List<String> fieldNames = new ArrayList<>();

		bindingResult.getGlobalErrors().forEach(globalError -> errorMessages.add(
				String.format("'%s'. Error in object '%s'", globalError.getDefaultMessage(), globalError.getObjectName())));

		bindingResult.getFieldErrors().forEach(fieldError -> errorMessages.add(
				String.format("'%s'. Field error in object '%s' on field '%s'. Rejected value '%s'",
						fieldError.getDefaultMessage(), fieldError.getObjectName(), fieldError.getField(),
						fieldError.getRejectedValue())));

		bindingResult.getFieldErrors().forEach(fieldError -> fieldNames.add(fieldError.getField()));

		final ErrorDto errorDto = new ErrorDto(ErrorType.VALIDATION_ERROR,
				String.join(System.lineSeparator(), errorMessages), requestURI);

		errorDto.setFieldNames(fieldNames);

		log.debug("Validation errors '{}' while processing REST service '{}'", errorMessages, requestURI);
		return new ErrorResponse(errorDto);
	}
}

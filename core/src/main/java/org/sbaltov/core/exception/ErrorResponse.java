package org.sbaltov.core.exception;

import org.sbaltov.core.exception.dto.ErrorDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Wrapper for {@link ErrorDto} to construct proper JSON.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
	private ErrorDto error;
}
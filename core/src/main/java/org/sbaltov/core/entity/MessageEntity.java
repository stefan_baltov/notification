package org.sbaltov.core.entity;

import javax.persistence.*;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@Table(name = "messages")
@NoArgsConstructor
@AllArgsConstructor
public class MessageEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "msg_type", length = 100)
	private String msgType;

	@Column(name = "address")
	private String address;

	@Column(name = "message")
	private String message;

	@Column(name = "sender")
	private String sender;

	@Column(name = "status", length = 100)
	private String status;

	@Column(name = "cou")
	private Integer cou;

	@Column(name = "created")
	private LocalDateTime created;

	@Column(name = "modified")
	private LocalDateTime modified;

	@Lob
	@Column(name = "log")
	private String log;
}
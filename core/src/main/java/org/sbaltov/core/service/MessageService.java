package org.sbaltov.core.service;

import org.sbaltov.common.dto.MessageDto;
import org.sbaltov.common.dto.MessageStatus;
import org.sbaltov.core.MessageDao;
import org.sbaltov.core.entity.MessageEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Service
@RequiredArgsConstructor
public class MessageService {
	private final MessageDao messageDao;

	@Transactional
	public Long createMessage(MessageDto dto) {
		MessageEntity entity = MessageEntity.builder().created(LocalDateTime.now())
				.message(dto.getMessage())
				.cou(1)
				.address(dto.getAddress())
				.msgType(dto.getType())
				.sender(dto.getSender())
				.status(MessageStatus.PENDING.name())
				.build();
		messageDao.save(entity);
		return entity.getId();
	}

	@Transactional
	public void saveMessage(Long id, String reason, MessageStatus status) throws Exception {
		MessageEntity entity = messageDao.findById(id).orElseThrow(() -> new Exception("Entity with ID " + id + " not found"));
		entity.setStatus(status.name());
		String log = entity.getLog() + "\n\r" + reason;
		entity.setLog(log);
		messageDao.save(entity);

	}

	public MessageDto getById(final Long id) throws Exception {
		MessageEntity entity = messageDao.findById(id).orElseThrow(() -> new Exception("Entity with ID " + id + " not found"));
		return MessageDto.builder()
				.message(entity.getMessage())
				.address(entity.getAddress())
				.id(entity.getId())
				.sender(entity.getSender())
				.type(entity.getMsgType())
				.status(MessageStatus.valueOf(entity.getStatus()))
				.build();
	}
}

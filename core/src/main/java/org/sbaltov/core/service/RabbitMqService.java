package org.sbaltov.core.service;

import static org.sbaltov.core.configuration.AppRabbitConfiguration.*;

import org.sbaltov.common.dto.MessageDto;
import org.sbaltov.common.dto.MessageStatus;
import org.sbaltov.core.configuration.RabbitMqSettings;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;

import lombok.extern.slf4j.Slf4j;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Slf4j
@Service
public class RabbitMqService {

	private RabbitTemplate rabbitTemplate;
	private AmqpAdmin amqpAdmin;
	private RabbitMqSettings settings;
	private MessageConverter messageConverter;
	private MessageService messageService;

  @Autowired
	public RabbitMqService(AmqpAdmin amqpAdmin, RabbitMqSettings settings, RabbitTemplate rabbitTemplate,
		  Exchange notificationDataExchange, MessageConverter messageConverter, MessageService messageService) {
		Properties properties = amqpAdmin.getQueueProperties(settings.getQueue());
		if (properties == null) {
			Queue smsQueue = QueueBuilder.durable(settings.getQueue())
					.withArgument("x-dead-letter-exchange", DLX_EXCHANGE_MESSAGES)
					.withArgument("x-dead-letter-routing-key", DL_ROUTING_KEY)
					.build();
			amqpAdmin.declareQueue(smsQueue );
			amqpAdmin.declareBinding(BindingBuilder.bind(smsQueue).to(notificationDataExchange).with("sms").noargs());
		}

		properties = amqpAdmin.getQueueProperties(settings.getQueue());
		log.info(properties.toString());
		this.rabbitTemplate = rabbitTemplate;
		this.amqpAdmin = amqpAdmin;
		this.settings = settings;

		this.messageConverter = messageConverter;
		this.messageService = messageService;
	}

	public void sendMessage(MessageDto dto) {
		rabbitTemplate.convertAndSend(dto.getType(), dto);
		Properties properties = amqpAdmin.getQueueProperties(settings.getQueue());
		log.info(properties.toString());
	}

	@RabbitListener(queues = QUEUE_MESSAGES_DLQ)
	public void processFailedMessages(Message message) throws Exception {
		log.info("Received failed message: {}", message.toString());
		MessageDto dto = (MessageDto) messageConverter.fromMessage(message);
		Long id = dto.getId();
		messageService.saveMessage(id, message.toString(),MessageStatus.ERROR);
	}

	@RabbitListener(queues = SUCCESS_QUEUE)
	public void processSuccessMessages(Message message) throws Exception {
		log.info("Received success message: {}", message.toString());
		MessageDto dto = (MessageDto) messageConverter.fromMessage(message);
		Long id = dto.getId();
		messageService.saveMessage(id, message.toString(),MessageStatus.SEND);
	}
}

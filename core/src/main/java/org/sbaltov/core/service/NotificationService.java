package org.sbaltov.core.service;

import javax.validation.ValidationException;

import org.sbaltov.common.dto.MessageDto;
import org.sbaltov.common.dto.MessageStatus;
import org.sbaltov.core.configuration.RabbitMqSettings;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService {

	private final RabbitMqSettings rabbitMqSettings;
	private final MessageService messageService;
	private final RabbitMqService rabbitMqService;

	public void sendMessage(MessageDto dto) throws Exception {
		validateMessageType(dto);
		Long id = messageService.createMessage(dto);
		dto.setId(id);
		dto.setStatus(MessageStatus.PENDING);
		log.info("Message to send {}", dto);
		rabbitMqService.sendMessage(dto);
	}

	private void validateMessageType(MessageDto dto) throws Exception {
		Boolean found = rabbitMqSettings.getTypes().stream().anyMatch(e -> e.toLowerCase().equals(dto.getType().toLowerCase()));
		if (!found) {
			throw new ValidationException("Unknown type: " + dto.getType());
		}
	}

	public MessageDto getMessageById(final Long id) throws Exception {
		return messageService.getById(id);
	}
}

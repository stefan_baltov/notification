package org.sbaltov.core;

import org.sbaltov.core.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Repository
public interface MessageDao extends JpaRepository<MessageEntity, Long> {}

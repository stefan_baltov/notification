package org.sbaltov.core.configuration;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

/**
 * TODO: Add documentation.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@Configuration
@RequiredArgsConstructor
public class AppRabbitConfiguration {

	public static final String DLX_EXCHANGE_MESSAGES = "dead-letter-exchange";
	public static final String QUEUE_MESSAGES_DLQ = "dead-letter-queue";
	public static final String DL_ROUTING_KEY = "dead-letter";
	public static final String SUCCESS_QUEUE = "success";
	private final RabbitMqSettings settings;

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(settings.getHost());
		connectionFactory.setUsername(settings.getUser());
		connectionFactory.setPassword(settings.getPassword());
		connectionFactory.setPort(settings.getPort());
		return connectionFactory;
	}

	@Bean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		template.setMessageConverter(jsonMessageConverter());
		configureRabbitTemplate(template);
		return template;
	}

	@Bean
	public MessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public DirectExchange notificationDataExchange() {
		DirectExchange exchange = new DirectExchange(settings.getExchange());
		return exchange;
	}

	/**
	 * @return the admin bean that can declare queues etc.
	 */
	@Bean
	public AmqpAdmin amqpAdmin() {
		RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory());
		return rabbitAdmin ;
	}

	public void configureRabbitTemplate(RabbitTemplate rabbitTemplate) {
		rabbitTemplate.setExchange(settings.getExchange());
	}

	/* dead letter settings */

	@Bean
	DirectExchange deadLetterExchange() {
		return new DirectExchange(DLX_EXCHANGE_MESSAGES);
	}

	@Bean
	Queue deadLetterQueue() {
		return QueueBuilder.durable(QUEUE_MESSAGES_DLQ).build();
	}

	@Bean
	Binding deadLetterBinding() {
		return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(DL_ROUTING_KEY);
	}
	// success confirmation

	@Bean
	public FanoutExchange successDataExchange() {
		FanoutExchange exchange = new FanoutExchange(SUCCESS_QUEUE);
		return exchange;
	}
	@Bean
	Queue successLetterQueue() {
		return QueueBuilder.durable(SUCCESS_QUEUE).build();
	}

	@Bean
	Binding successLetterBinding() {
		return BindingBuilder.bind(successLetterQueue()).to(successDataExchange());
	}
}

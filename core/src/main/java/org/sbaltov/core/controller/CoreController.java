package org.sbaltov.core.controller;

import javax.validation.Valid;

import org.sbaltov.common.dto.MessageDto;
import org.sbaltov.core.service.NotificationService;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;

/**
 * Controller for receiving messages.
 *
 * @author sbaltov.
 * @version 0.1.0
 */
@RestController
@RequestMapping(CoreController.MAPPING)
@RequiredArgsConstructor
public class CoreController {
	static final String MAPPING = "/core";
	static final String MAPPING_BY_ID = "/{id}";

	private final NotificationService service;

	@PostMapping
	public MessageDto sendMessage(@RequestBody @Valid final MessageDto message) throws Exception {
		service.sendMessage(message);
		return message;
	}

	@GetMapping(MAPPING_BY_ID)
	public MessageDto getMessageById(@PathVariable Long id) throws Exception {
		return service.getMessageById(id);
	}
}

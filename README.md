 #Design and implementation of notification system


![Architecture](./arch_diagram.png?raw=true "Architecture")

Clients send messages to core microservice.
The Post endpoint is "/core".

The Message is:
````
{
"address": "string",
"message": "string",
"sender": "string",
"type": "string"
}
````
After the message is send the clients will get confirmation with message ID.
Then clients could pull periodicaly core service on `GET /core/{id}` to get a status of the message.

 The current state of implementation implements a skeleton of sms handler service. To implement other types:
 1. Add new type to core configuration in `rabitmq.settins.types`
 2. Create message handler microservice, using sms handler as prototype and implement `sendMessage` method from 
    `AbstractMessageSender`. Set the type of handled messages in config.
 3. Deploy it

## Horizontally scalable
The system is easily scalable, just deploy new instance of needed microservice

## How to run
### prerequisites
* Java 11
* Maven
* Docker and Docker compose

----------------

1. Clone git repository
2. Enter main directory `cd notification`
3. Enter `deploy` directory - `cd deploy`
4. Run `./deploy.sh`
5. After a few minutes, open the browser at: `http://localhost:8082/swagger-ui/#/core-controller/`